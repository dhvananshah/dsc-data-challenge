# coding: utf-8

import pandas as pd     #data analysis toolkit
import operator
import numpy as np


#Loading the dataset into a dataframe
with open('../data/pageViews.csv','r') as f:
    data = pd.read_csv(f)


<<<<<<< HEAD

#Converting the 'collector_tstamp' to datetime type from string.
data['collector_tstamp'] = pd.to_datetime(data['collector_tstamp'])


# Create a new copy of the data and added a new column 'next_event_id' that would store the next page's event_id.
data_transformed = data.copy()
=======
#Create new column 'next_event_id' and assign None values to all the rows.
>>>>>>> 3d48c8fc327bec841e9ee7d32171c48c00f58fb2
data_transformed['next_event_id'] = None


#Creating the transformed data (Linked Data)
for user_id in data_transformed['domain_userid'].unique():

    user_data = data_transformed[data_transformed['domain_userid'] == user_id]
    user_data = user_data.sort_values('collector_tstamp')
    
    for i in range(len(user_data)-1):

        #Getting the index of the corresponding mapping
        row_condition = data_transformed['event_id'] == user_data.iloc[i].event_id
        dt_index = data_transformed[row_condition].index.tolist()[0]
        
        #Setting current node to point to the next node
        data_transformed.loc[dt_index,'next_event_id'] = user_data.iloc[i+1].event_id
    
    #Setting the last node to Null
    row_condition = data_transformed['event_id'] == user_data.iloc[len(user_data)-1].event_id
    dt_index = data_transformed[row_condition].index.tolist()[0]
    
    data_transformed.loc[dt_index,'next_event_id'] = 'Null'

#Writing the transformed data to csv
data_transformed.to_csv('../data/transformed_data.csv',index=False)