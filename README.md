#DSC Data Challenge Submission#

### Language:  
Python 2.7

### Dependencies:  
pandas >= 0.19.2

### Execution  
python scripts/dsc_data_challenge.py

### Output  
data/transformed_data.csv

### Analysis  
Detailed walk-through and analysis is given in the IPython Notebook, please view **dsc_data_challenge-notebook.pdf**.  
Or download **DSC_Data_Challenge-Notebook.html** and load it on a browser to see the notebook.